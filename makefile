pkg := oldstandard
ver  := $(shell grep '\\edef\\pkgver' $(pkg).tex | grep -Eo '[0-9]?\.[0-9]?')
TEXMFDIR := $(shell kpsewhich -expand-var='$$TEXMFHOME')
HOMEDIR := $$HOME

doc: clean
	lualatex --shell-escape $(pkg).tex
	biber $(pkg)
	lualatex --shell-escape $(pkg).tex
	lualatex --shell-escape $(pkg).tex

all: clean doc

package: clean doc
	mkdir $(pkg)
	cp *.{txt,md,tex,pdf} $(pkg)
	cp otf/*.otf $(pkg)
	tar czf $(pkg)-$(ver).tar.gz $(pkg)

clean:
	git clean -df
	pandoc README.md -o about.html

.PHONY: doc all package clean
