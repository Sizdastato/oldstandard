\RequirePackage{filecontents}
\begin{filecontents*}{\jobname.bib}
@software{arabluatex,
  title =	 {The arabluatex package},
  titleaddon =	 {Arab\TeX\ for Lua\LaTeX},
  author =	 {Alessi, Robert},
  url =		 {https://ctan.org/pkg/arabluatex},
  version =	 {1.17}
}
@software{babel,
  title =	 {The Babel package},
  titleaddon =	 {Multilingual support for Plain TeX or LaTeX},
  author =	 {Bezos López, Javier and Braams, Johannes L.},
  url =		 {http://www.ctan.org/pkg/babel},
  version =	 {3.33}
}
@software{oldstandard,
  title =	 {The OldStandard package},
  titleaddon =	 {Old Standard: A Unicode Font for Classical and
                  Medieval Studies},
  author =	 {Kryukov, Alexey},
  editor =	 {Lečić, Nikola and Tennent, Bob},
  editortype =	 {compiler},
  url =		 {http://www.ctan.org/pkg/oldstandard},
  version =	 {2.3}
}
\end{filecontents*}
\documentclass[letterpaper]{article}
\usepackage[no-math]{fontspec}
\usepackage{fontspec}
\usepackage[greek.ancient,english]{babel}
\babeltags{grc = greek}

\babelfont{rm}[Path = otf/,
ItalicFont={OldStandard-Italic.otf},
BoldFont={OldStandard-Bold.otf},
BoldItalicFont={OldStandard-BoldItalic.otf}]{OldStandard-Regular.otf}

\babelfont[greek]{rm}[Path = otf/,
RawFeature={+ss05;+ss06},
ItalicFont={OldStandard-Italic.otf},
BoldFont={OldStandard-Bold.otf},
BoldItalicFont={OldStandard-BoldItalic.otf}]{OldStandard-Regular.otf}

\babelfont{tt}{CMU Typewriter Text}

\newlength\defaultparindent
\setlength\defaultparindent{\parindent}
\usepackage{dtxdescribe}
\setlength\parindent{\defaultparindent}

\usepackage[xindy]{imakeidx}
\indexsetup{noclearpage}
\makeindex

\usepackage{latexcolors}
\usepackage{csquotes}
\usepackage{varioref}
\usepackage{hyperref}
\hypersetup{unicode=true, linktocpage=true, colorlinks,
  allcolors=cinnamon, pdfauthor={Robert Alessi}, pdftitle={Old
    Standard}}
\usepackage{uri}

\usepackage{enumitem}
\setlist{nosep}
\setlist[itemize]{label=\textendash}
\setlist[enumerate,1]{label=(\alph*)}
\setlist[enumerate,2]{label=\roman*.}
\usepackage{metalogox}
\usepackage{lettrine}
\usepackage{setspace}

\usepackage{relsize}
\usepackage{tikz}
\usepackage[breakable, skins, xparse, minted]{tcolorbox}
\tcbset{colback=white, boxrule=.15mm, colframe=cinnamon, breakable}
\newtcblisting{example}{minted options={linenos, numbersep=0mm,
    fontsize=\smaller}}
\newtcblisting{code}{minted options={linenos, numbersep=0mm,
    fontsize=\smaller}, listing only}

\usepackage[fullvoc]{arabluatex}
\usepackage[style=oxnotes-inote]{biblatex}
\DeclareFieldFormat{postnote}{\mkpageprefix[pagination][\mknormrange]{#1}}
\addbibresource{oldstandard.bib}
\usepackage[citecmd=autocite,defaultindex=none]{icite}
\bibinput{oldstandard}

\usepackage{cleveref}

\usepackage[toc]{multitoc}

\edef\pkgver{2.4}
\edef\pkgdate{2019/07/25}
\title{\mdseries\tcbox[colframe=black, enhanced, tikznode, drop
  lifted shadow, colback=white, boxrule=.25mm]%
  {\textsc{Old Standard}\\
    \Large
    A Unicode Font for Classical and Medieval Studies\\
    \large Based on Alexey Kryukov's original \emph{Old Standard}\\
    \large v\pkgver -- \pkgdate}}

\author{Robert Alessi \\
\href{mailto:alessi@robertalessi.net?Subject=arabluatex package}%
{\texttt{alessi@robertalessi.net}}}
\date{}

\begin{document}
\maketitle
\footnotesize
\tableofcontents
\normalsize

\begin{abstract}
  This font is just the same as Alexey Kryukov's beautiful \emph{Old
    Standard}. In comparison to the previous releases of \emph{Old
    Standard}, it includes new letters and some corrections.
\end{abstract}

\section{License}
\label{sec:license}
Copyright \textcopyright\ 2006--2011, Alexey Kryukov
(\href{mailto:amkryukov@gmail.com}{amkryukov@gmail.com}), without
Reserved Font Names.
\\
Copyright \textcopyright\ 2019, Robert Alessi
(\href{mailto:alessi@robertalessi.net}{alessi@robertalessi.net}), without
Reserved Font Names.

Please send error reports and suggestions for improvements to Robert
Alessi:
\begin{itemize}
\item email: \mailto[oldstandard package]{alessi@roberalessi.net}
\item website: \url{http://www.robertalessi.net/oldstandard}
\item development: \url{http://git.robertalessi.net/oldstandard}
\item comments, feature requests, bug reports:
\url{https://gitlab.com/ralessi/oldstandard/issues}
\end{itemize}

This Font Software is licensed under the SIL Open Font License,
Version 1.1. This license is available with a FAQ at:
\url{http://scripts.sil.org/OFL}

\section{History}
\label{sec:history}
\emph{Old Standard} is a remarkable creation of Alexey Kryukov,
inspired by a typeface most commonly used in books printed in the late
\textsc{xix}\textsuperscript{th} and early
\textsc{xx}\textsuperscript{th} century. The source files, which can
be found online,\footnote{See
  \url{https://github.com/akryukov/oldstand}} have been published
under the terms of the OFL license (see above,
\vref{sec:license}). However, at the time of writing, the latest
update dates back to Aug.\ 12, 2013. To be more precise, all of the
five \enquote*{commits} the writer was able to see were pushed on the
very same day. Since then, two \enquote*{pull requests} dating back to
2017 have remained unanswered. It is therefore to be feared that the
project has been abandoned.  To date, this release of \emph{Old
  Standard} has been published by Nikola Lečić and Bob Tennent and is
available on CTAN and {\TeX}Live with a style file.\footnote{See
  \url{https://ctan.org/pkg/oldstandard}}

Being unable himself to contact the author, the writer, while in need
to have new letters included in \emph{Old Standard} and some issues
addressed, took the decision to make a new release \emph{Old Standard}.

\paragraph{Important disclaimer}
The writer is very far from being able to design glyphs \emph{ex
  nihilo}. That aside, he has some limited knowledge in the use of
FontForge, and, as a classicist, he is able to scrutinize how features
operate and if they operate as expected.

\section{Documentation}
\label{sec:documentation}
No documentation is associated with this release of \emph{Old
  Standard} as every item of the original extensive documentation
applies. The reader should refer to it.\icite{oldstandard}

\section{Additions and corrections provided}
\label{sec:addit-corr-prov}
This release of \emph{Old Standard} includes new letters and some
corrections:
\begin{enumerate}
\item Small capitals for Roman, Greek and Cyrillic letters, in all
  three styles, Regular, Italic and Bold have been added. Small
  capitals, which are missing from \emph{Old Standard}, were already
  in use a century ago in fine books which used font faces very
  similar to \emph{Old Standard}. Typical use cases of small capitals
  were headers, current headings and in some books proper names.
\item \emph{For the time being}, a bold italic shape has been
  auto-generated. Of course, auto-generating shapes is not a
  satisfactory solution. However, it is better than using the
  font loader to emulate bold shapes. A real bold italic shape is
  planned in the versions of \emph{Old Standard} to come.
\item The letter G with caron above, that is: Ǧ (\verb|U+01E6|,
  uppercase) and ǧ (\verb|U+01E7|, lowercase) has been added. It is
  the only character missing from \emph{Old Standard} that is needed
  in some of the accepted standards of romanization of classical
  Arabic. See for references the current documentation of the
  \textsf{arabluatex} package.\footnote{\icite{arabluatex}[cite], sect{.}
    \enquote{Transliteration}.}
\item Additionally, this release corrects the \verb|+ss06| feature
  which is supposed to distinguish between regular and
  \enquote*{curled} beta (β/ϐ) and to print \enquote*{curled} beta
  (\verb|U+03D0|) in medial position. This feature worked in most
  cases with the previous releases. However, it failed if the beta was
  preceded by a vowel with an acute accent taken from the Greek
  extended Unicode block.
\end{enumerate}

\section{Usage}
\label{sec:usage}
\emph{Old Standard} works with \TeX\ engines that directly support
OpenType features such as \XeTeX\ and \LuaTeX.

It is loaded with \pkg{fontspec} like so:---
\begin{code}
  \usepackage{fontspec}
  \setmainfont{Old Standard}
\end{code}

\paragraph{Small capitals}
Small capitals have been added for the following languages or
transcription schemes: French, German, Italian, Spanish, unaccented
Greek, basic Russian and Arabic \enquote*{DMG}.

The following two examples demonstrate the use of small capitals:---
\begin{tcblisting}{minted language=latex, title=Initials, minted
    options={fontsize=\smaller, linenos, numbersep=0mm,
      highlightlines={7}}}
  \begin{center}
    CHAPTER I

    MR.\ SHERLOCK HOLMES
  \end{center}
  
  \lettrine[loversize=0.2]{M}{r.\ Sherlock Holmes}, who was usually
  very late in the mornings, save upon those not infrequent occasions
  when he stayed up all night, was seated at the breakfast table. I
  stood upon the hearth-rug and picked up the stick which our visitor
  had left behind him the night before.  It was a fine, thick piece of
  wood, bulbous-headed, of the sort which is known as a
  \enquote{Penang lawyer.} Just under the head was a broad silver
  band, nearly an inch across. \enquote{To James Mortimer, M.R.C.S.,
    from his friends of the C.C.H.,} was engraved upon it, with the
  date \enquote{1884.} It was just such a stick as the old-fashioned
  family practitioner used to carry—dignified, solid, and reassuring.
\end{tcblisting}

\begin{tcblisting}{minted language=latex, title=Headings, minted
    options={linenos, numbersep=0mm, fontsize=\smaller,
      highlightlines={11}}}
  \doublespacing
  \begin{center}
    \textlarger{PART SECOND}.

    ETYMOLOGY OR THE PART OF THE SPEECH.
    
    \rule{1in}{0.4pt}

    I. THE VERB, \arb{al-fi`lu}.

    A. \textsc{General View}.

    1. \emph{The Forms of the Triliteral Verb}.
  \end{center}
\end{tcblisting}

\paragraph{The letter \enquote*{ǧ}} It is used notably to print
romanized Arabic. \emph{Old Standard} now features this letter in all
of the three styles (Regular, Italic and Bold):---
\begin{tcblisting}{minted language=latex, minted
    options={fontsize=\smaller, linenos, numbersep=0mm,
      highlightlines={3,6,9}}}  
  \begin{arab}[trans]
    \begin{center}
      \textbf{da^gA^gaTu \uc{'a}bI 'l-\uc{h}u_dayli 'l-\uc{`a}llAfi}
    \end{center}
    kAna \uc{'a}bU 'l-\uc{h}u_dayli 'ahd_A 'il_A \uc{m}uwaysiN
    da^gA^gaTaN. wa-kAnat da^gA^gatu-hu 'llatI 'ahdA-hA dUna mA kAna
    yuttaxa_du li-\uc{m}uwaysiN.

    (\uc{al-^gA.hi.zu}, \aemph{\uc{k}itAbu 'l-\uc{b}u_halA'i})
  \end{arab}
\end{tcblisting}

\paragraph{\texttt{+ss06} OpenType feature} It is commonly believed
that all Greek vowels with acute accent taken from the Greek Extended
Unicode block \verb|1F00–1FFF| along with standalone acute accents
were duplicated from the Greek and Coptic Unicode block. Affected
characters from the Greek Extended Unicode block (\verb|0370–03FF|)
follow: \textgrc{ά, έ, ή, ί, ό, ύ, ώ, Ά, Έ, Ή, Ί, Ό, Ύ, Ώ, ΐ, ΰ, ´,
  ΅}. The counterparts of these letters in the Greek and Coptic
Unicode block are vowels with \emph{tonoi}.

However, strictly speaking, \emph{tonos} is not to be mistaken for
\enquote*{acute}: that is for sure, as \emph{tonos} was introduced as
a result of a reform to denote a tone, namely a stress on some vowels,
and not a pitch, namely a rising and falling voice on accented vowels.
Confusion began when the Greek government decreed that \emph{tonos}
shall be the acute.  From what the writer could see, many Greek fonts
originally reflected the distinction between \emph{tonos} and acute.
But nowadays, they simply mix them up.  As a result of this confusion,
in \emph{Old Standard}, vowels with acute were simply missing from the
Greek Extended Block. All of them, including the standalone accents,
have been restored. Furthermore, the rule that instructed to absorb
vowels with acute into vowels with \emph{tonos} has been removed.

Since assigning vowels with \emph{tonos} and vowels with acute to the
same code points is clearly unacceptable even if the glyphs are
identical, it is now possible in \emph{Old Standard} to input all
accented vowels from the Greek Extended Unicode block exclusively and
have the substitution rules applied at the same time, as shown by the
example that follows:---
\begin{tcblisting}{minted language=latex, minted
    options={fontsize=\smaller, linenos, numbersep=0mm,
      highlightlines={9-10}}}
  \begin{grc}
    \begin{center}
      \textlarger{ΙΠΠΟΚΡΑΤΟΥΣ ΕΠΙΔΗΜΙΩΝ ΤΟ ΔΕΥΤΕΡΟΝ}.

      ΤΜΗΜΑ ΠΡΩΤΟΝ.
    \end{center}
    
    \textbf{1.} Ἄνθρακες θερινοὶ ἐν Κραννῶνι· ὗεν ἐν καύμασιν ὕδατι
    λάβρῳ δι’ ὅλου καὶ ἐγίνετο μᾶλλον νότῳ, [καὶ] ὑπογίνονται μὲν ἐν
    τῷ δέρματι ἰχῶρες· ἐγκαταλαμβανόμενοι δέ, θερμαίνονται, καὶ
    κνησμὸν ἐμποιέουσιν· εἶτα φλυκταινίδες ὥσπερ πυρίκαυστοι
    ἐπανίσταντο καὶ ὑπὸ τὸ δέρμα καίεσθαι ἐδόκεον.
  \end{grc}
\end{tcblisting}

\subsection{Using \emph{Old Standard} in multilingual
  documents}
\label{sec:using-old-standard}
\pkg{babel} provides a high level interface on top of \pkg{fontspec}
to select fonts depending on the languages to be used.\icite[For more
information, the reader should refer to][10,24]{babel} As an example,
here is how \emph{Old Standard} has been loaded in the preamble of
this document to be compiled with \LuaLaTeX:---
\begin{code}
  \usepackage[no-math]{fontspec}
  \usepackage{fontspec}
  \usepackage[greek.ancient,english]{babel}
  \babeltags{grc = greek}
  
  \babelfont{rm}[BoldItalicFont={Old Standard Italic},
  BoldItalicFeatures={RawFeature={+embolden=2}}]{Old Standard}
  
  \babelfont[greek]{rm}[RawFeature={+ss05;+ss06},
  BoldItalicFont={Old Standard Italic},
  BoldItalicFeatures={RawFeature={+embolden=2}}]{Old Standard}
\end{code}

Then, once \emph{Old Standard} has been loaded with \cs{babelfont}
properly,
\begin{enumerate}
\item \cs{textgrc}\marg{Greek text} can be used for short insertions
  of Greek text.
\item \verb|\begin{grc}| ... \verb|\end{grc}| can be used for
  inserting running paragraphs of Greek text.
\end{enumerate}

\end{document}
